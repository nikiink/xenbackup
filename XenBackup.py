# This script reads virtual machines to backup from an xml
# configuration file by default named XenBackup.xml.
# The configuration file can be given as parameter -config, for example
#
#   XenBackup.ps1 -config "XenBackup-test.xml"
#
# [If not given is used the file XenBackup.xml]
#
# The values of maxBackups and backupDir
# can be overridden on specific machine.
# This is an example:
#
# <?xml version="1.0"?>
# <XenBackup>
#     <backupDir>C:\BACKUPS</backupDir>
#     <maxBackups>2</maxBackups>
#     <logFile>XenBackup.log</logFile>
#     <server host="xenserver001" user="root" password="password">
#        <vm name="TEST2"/>
#        <!-- override maxBackups and backupDir -->
#        <vm name="Test VM" maxBackups="2" backupDir="C:\BACKUPS2"/>
#        <vm name="Ubuntu Xenial" />
#        <vm name="Knoppix" />
#     </server>
#     <server host="xenserver002" user="root" password="password">
#        <vm name="Knoppix2" />
#     </server>
# </XenBackup>

import argparse
import datetime
import glob
import logging
import os
import xml.etree.ElementTree as et
import XenAPI

parser = argparse.ArgumentParser(description='Backup Xenserver Virtual Machines.')
parser.add_argument('--config', default="XenBackup.xml", help='Configuration file')
args = parser.parse_args()

configFile = args.config

print("READING CONFIGURATION FROM FILE: " + configFile)

configXml = et.parse(configFile)
backupDir = configXml.find("backupDir").text
maxBackups = configXml.find("maxBackups").text
logFile = configXml.find("logFile").text

logging.basicConfig(filename=logFile, level=logging.DEBUG)


def go():
    logging.info("CONFIGURATION LOADED FROM: " + configFile)
    for server in configXml.iterfind("server"):
        #Connect-XenServer -NoWarnCertificates -NoWarnNewCertificates -url ("https://" + server.get("host")) server.get("user") server.get("password")
        session = XenAPI.Session("https://" + server.get("host"))
        session.xenapi.login_with_password(server.get("user"), server.get("password"), "1.0", "XenBackup.py")

        logging.info("CONNECTED TO SERVER: " + server.get("host"))

        for vm in server.iterfind("vm"):
            logging.info("BACKUPING VM: " + vm.get("name"))
            backup_vm(session, server,vm)
            clean_old_backups(vm)

        session.xenapi.session.logout()
        logging.info("DISCONNECTED FROM SERVER: " + server.get("host"))


def backup_vm(session, server, vm):
    bckdir = backupDir
    if vm.get("backupDir"):
        bckdir = vm.get("backupDir")

    print("BCK DIR: " + bckdir)
    timestamp = datetime.datetime.now().strftime('%Y-%m-%dT%H%M%S')
    snapshot_name = vm.get("name") + "_bck_" + timestamp

    #Create temporary snapshot for hot backup
    ###Invoke-XenVM -Name $vm.name -XenAction Snapshot -NewName $snapshot_name
    ###$snapshot = Get-XenVM -Name $snapshot_name
    snapshot = session.xenapi.VM.snapshot(vm.get("name"), snapshot_name)

    #Set is-a-template and ha-always-run to false
    ###Set-XenVM -Uuid $snapshot.uuid -IsATemplate $false -HaAlwaysRun $false
    session.xenapi.VM.set_is_a_template(snapshot, False)
    session.xenapi.VM.set_ha_always_run(snapshot, False)

    #Export snapshot
    set_temp_dir(bckdir)
    ###Export-XenVM -Uuid $snapshot.uuid -XenHost $server.host -Path ($bckdir + "\" + $vm.name + "-${timestamp}.xva")

    #Remove snapshot
    ###Remove-XenVM -Uuid $snapshot.uuid


def set_temp_dir(newDir):
    pass


def clean_old_backups(vm):
    maxbkps = int(maxBackups)
    bckdir = backupDir
    if vm.get("maxBackups"):
        maxbkps = int(vm.get("maxBackups"))
    if vm.get("backupDir"):
        bckdir = vm.get("backupDir")

    logging.info("MAXBACKUPS: " + str(maxbkps))

    vmbackupfiles = glob.glob(bckdir + "/" + vm.get("name") + "-*.xva")
    vmbackupfiles.sort(reverse=True)
    count = 0
    for f in vmbackupfiles:
        count += 1
        logging.info(count)
        if count > maxbkps:
            logging.info("DELETING " + f)
            os.remove(f)
        else:
            logging.info("LEAVING " + f)

go()

